----------------------------------One of the staging table(with derived columns from columns of raw table) from raw table- raw_ce_ct_mapping------------------------------------------------------

drop table stg_ce_ct_mapping;
create table stg_ce_ct_mapping
(
ce_ct_cost_type_key string,
cost_type_description string,
c_ceh_mapping string,
c_ceh_mapping_level string,
ce_ct_cost_type string,
level_mapping_joined string
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
   );


truncate table stg_ce_ct_mapping;
insert into table stg_ce_ct_mapping
select 
cost_type as ce_ct_cost_type_key,
cost_type_description,
c_ceh_mapping,
c_ceh_mapping_level,
case
 when c_ceh_mapping not in ('180600', '180700', '180400') then 'primary_cost'
 when c_ceh_mapping = '180400' then 'bt_core_project'
 else NULL
end as ce_ct_cost_type,
case 
 when 
  (trim(c_ceh_mapping_level) = '' or upper(c_ceh_mapping_level) = 'NULL' or c_ceh_mapping_level is NULL) 
   AND 
  (trim(c_ceh_mapping) = '' or upper(c_ceh_mapping) = 'NULL' or c_ceh_mapping is NULL)
 then NULL
 else concat(c_ceh_mapping_level,'_',c_ceh_mapping) end as level_mapping_joined
from raw_ce_ct_mapping;
