-------------------Steps to reach for one of the final tables- bt_unit_hierarchy
-------------------Step 1: Creating distinct_btunit---------------------------------------------------

create table distinct_btunit
(
 BT_Unit         string,     
 BT_Unit_Text    string    
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES 
(
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
);

insert into table distinct_btunit 
select BIIT_BT_Unit AS BT_Unit, Min(BIIT_BT_Unit_Text) AS BT_Unit_Text
from stg_BIIT
group by BIIT_BT_UNIT;

----------------------------###################################
--------------------------------Step 2: Creating distinct_prjbt_bt_unit-----------------------------------------------------------

create table distinct_prjbt_bt_unit
(
 BT_Unit         string,     
 BT_Unit_Text    string    
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES 
(
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
);


insert into table distinct_prjbt_bt_unit
select PRJBT_BT_Unit as BT_Unit, PRJBT_BT_Unit_text as BT_Unit_Text
from stg_prj_bt_responsibility
group by
PRJBT_BT_Unit, PRJBT_BT_Unit_text;
-------------------------#####################################
-----------------------------------------------Step 3: Creating combined table prj_bt_unit_combined------------------------------------------------

create table prj_bt_unit_combined
(
 BT_Unit         string,     
 BT_Unit_Text    string    
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES 
(
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
);


insert into table prj_bt_unit_combined
select distinct trim(split(BT_Unit,'[:]')[0]) as BT_Unit,trim(BT_Unit_Text) as BT_Unit_Text from (
select BT_Unit,BT_Unit_Text
from distinct_btunit
union
select
BT_Unit,BT_Unit_Text
from distinct_prjbt_bt_unit )abc;

-------------------------------Step 4: creation of final table bt_unit_hierarchy--------------------------------------------------------------

create table bt_unit_hierarchy
(
 BT_Unit         string,     
 BT_Unit_Text    string,
 BT_Unit_Overall string,
 BT_Unit_X       string,
 BT_Unit_XX      string,
 BT_Unit_XXX     string
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES 
(
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
);


insert into table bt_unit_hierarchy 
select 
 BT_Unit as BT_Unit
,BT_Unit_Text as BT_Unit_Text 
,split(BT_Unit,'[-]')[0] as BT_Unit_Overall
,case 
WHEN length(BT_Unit)=4 then BT_Unit  
when length(BT_Unit)=5 then substr((BT_Unit),0,4)   
when length(BT_Unit)=6 then  substr((BT_Unit),0,4)    
else NULL
END as BT_Unit_X
,case 
WHEN length(BT_Unit)=5 then substr((BT_Unit),0,5)
when length(BT_Unit)=6 then substr((BT_Unit),0,5)
else NULL
END as BT_Unit_XX
,case 
WHEN length(BT_Unit)=6 then substr((BT_Unit),0,6)
else NULL 
end as BT_Unit_XXX                      
from prj_bt_unit_combined;

