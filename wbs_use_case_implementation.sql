------------Steps to create one of the final table final_gfd_project_wbs_flexible_joined-------------------------------------
------------Step 1: Creation of stg_gfd_project_wbs_flexible from raw_gfd_project_wbs_flexible-----------------------

CREATE TABLE stg_gfd_project_wbs_flexible ( 
 gfdwbs_calendar_year_month_raw               STRING
,gfdwbs_calendar_year_month                   STRING
,gfdwbs_sender_receiver                       STRING
,gfdwbs_project_definition                    STRING
,gfdwbs_project_description                   STRING
,gfdwbs_cost_element                          STRING
,gfdwbs_cost_element_description              STRING
,gfdwbs_co_doc_header_txt                     STRING
,gfdwbs_co_doc_line_item_txt                  STRING
,gfdwbs_vendor                                STRING
,gfdwbs_vendor_description                    STRING
,gfdwbs_purchasing_document                   STRING
,gfdwbs_text_of_purchase_ord                  STRING
,gfdwbs_partner_cost_center                   STRING
,gfdwbs_partner_cost_center_description       STRING
,gfdwbs_partner_internal_ord                  STRING
,gfdwbs_partner_internal_ord_description      STRING
,gfdwbs_partner_wbs_element                   STRING
,gfdwbs_partner_wbs_element_description       STRING
,gfdwbs_wbs_element                           STRING
,gfdwbs_wbs_element_description               STRING
,gfdwbs_actual_costs                          STRING
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   "serialization.null.format"="NULL"
   );



insert into table stg_gfd_project_wbs_flexible
select
gfdwbs_calendar_year_month_raw as  gfdwbs_calendar_year_month_raw                 
,concat(substr((gfdwbs_calendar_year_month_raw),4,4),substr((gfdwbs_calendar_year_month_raw),0,2)) as gfdwbs_calendar_year_month
,gfdwbs_sender_receiver   as   gfdwbs_sender_receiver          
,gfdwbs_project_definition   as  gfdwbs_project_definition          
,gfdwbs_project_description    as   gfdwbs_project_description 
,gfdwbs_cost_element      as     gfdwbs_cost_element  
,gfdwbs_cost_element_description     as  gfdwbs_cost_element_description
,gfdwbs_co_doc_header_txt  as    gfdwbs_co_doc_header_txt
,gfdwbs_co_doc_line_item_txt   as gfdwbs_co_doc_line_item_txt
,gfdwbs_vendor     as gfdwbs_vendor
,gfdwbs_vendor_description  as gfdwbs_vendor_description
,gfdwbs_purchasing_document   as gfdwbs_purchasing_document
,gfdwbs_text_of_purchase_ord  as  gfdwbs_text_of_purchase_ord
,gfdwbs_partner_cost_center  as gfdwbs_partner_cost_center
,gfdwbs_partner_cost_center_description as gfdwbs_partner_cost_center_description
,gfdwbs_partner_internal_ord  as gfdwbs_partner_internal_ord
,gfdwbs_partner_internal_ord_description as gfdwbs_partner_internal_ord_description
,gfdwbs_partner_wbs_element  as gfdwbs_partner_wbs_element
,gfdwbs_partner_wbs_element_description as gfdwbs_partner_wbs_element_description
,gfdwbs_wbs_element   as gfdwbs_wbs_element
,gfdwbs_wbs_element_description as gfdwbs_wbs_element_description
,gfdwbs_actual_costs as gfdwbs_actual_costs
from raw_gfd_project_wbs_flexible;
-----Comment during code review: We may use the filter condition done at later stage, in this stage itself.
-----where stg_gfd_project_wbs_flexible.GFDWBS_WBS_ELEMENT != "Result" and stg_gfd_project_wbs_flexible.gfdwbs_partner_internal_ord != "Result"
-----and stg_gfd_project_wbs_flexible.gfdwbs_partner_wbs_element != "Result";


-----Step 2: Creation of table wbs_stg1-----------------------------------


create table wbs_stg1
(
 gfdwbs_calendar_year_month_raw           string,     
 gfdwbs_calendar_year_month               string,     
 gfdwbs_sender_receiver                   string,     
 gfdwbs_project_definition                string,     
 gfdwbs_project_description               string,     
 gfdwbs_cost_element                      string,     
 gfdwbs_cost_element_description          string,     
 gfdwbs_co_doc_header_txt                 string,     
 gfdwbs_co_doc_line_item_txt              string,     
 gfdwbs_vendor                            string,     
 gfdwbs_vendor_description                string,     
 gfdwbs_purchasing_document               string,     
 gfdwbs_text_of_purchase_ord              string,     
 gfdwbs_partner_cost_center               string,     
 gfdwbs_partner_cost_center_description   string,     
 gfdwbs_partner_internal_ord              string,     
 gfdwbs_partner_internal_ord_description  string,     
 gfdwbs_partner_wbs_element               string,     
 gfdwbs_partner_wbs_element_description   string,     
 gfdwbs_wbs_element                       string,     
 gfdwbs_wbs_element_description           string,     
 gfdwbs_actual_costs                      string,
ce_l1                                     string,
ce_desc_l1                                string,
ce_l2                                     string,
ce_desc_l2                                string,
ce_l3                                     string,
ce_desc_l3                                string,
ce_l4                                     string,
ce_desc_l4                                string,
ce_l5                                     string,
ce_desc_l5                                string,
ce_prefix                                 string,
ce_suffix                                 string,
ce                                        string,
ce_desc                                   string,
cost_type                                 string,
ce_ct_level_mapping_joined                string
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES 
(
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
);


insert into table wbs_stg1
select 
stg_gfd_project_wbs_flexible.*,
stg_ce_h_ce_ct.ce_l1,
stg_ce_h_ce_ct.ce_desc_l1,
stg_ce_h_ce_ct.ce_l2,
stg_ce_h_ce_ct.ce_desc_l2,
stg_ce_h_ce_ct.ce_l3,
stg_ce_h_ce_ct.ce_desc_l3,
stg_ce_h_ce_ct.ce_l4,
stg_ce_h_ce_ct.ce_desc_l4,
stg_ce_h_ce_ct.ce_l5,
stg_ce_h_ce_ct.ce_desc_l5,
stg_ce_h_ce_ct.ce_prefix,
stg_ce_h_ce_ct.ce_suffix,
stg_ce_h_ce_ct.ce,
stg_ce_h_ce_ct.ce_desc,
case 
when trim(gfdwbs_project_definition) != '' or upper(gfdwbs_project_definition) != 'NULL' or gfdwbs_project_definition IS  NOT NULL then 
(case when cost_type='bt_core_project' then '' else cost_type end)
 end as cost_type,
ce_ct_level_mapping_joined
from stg_gfd_project_wbs_flexible 
left outer join 
stg_ce_h_ce_ct on 
stg_gfd_project_wbs_flexible.gfdwbs_cost_element = stg_ce_h_ce_ct.ce
where stg_gfd_project_wbs_flexible.GFDWBS_WBS_ELEMENT != "Result" and stg_gfd_project_wbs_flexible.gfdwbs_partner_internal_ord != "Result
and stg_gfd_project_wbs_flexible.gfdwbs_partner_wbs_element != "Result";////////////////////Comment during code review: To move the filter condition to stg_gfd_project_wbs_flexible stage.

----Step 3: Creation of final WBS table final_gfd_project_wbs_flexible_joined---------------------------------------------

create table final_gfd_project_wbs_flexible_joined
(
 gfdwbs_calendar_year_month_raw           string,     
 gfdwbs_calendar_year_month               string,     
 gfdwbs_sender_receiver                   string,     
 gfdwbs_project_definition                string,     
 gfdwbs_project_description               string,     
 gfdwbs_cost_element                      string,     
 gfdwbs_cost_element_description          string,     
 gfdwbs_co_doc_header_txt                 string,     
 gfdwbs_co_doc_line_item_txt              string,     
 gfdwbs_vendor                            string,     
 gfdwbs_vendor_description                string,     
 gfdwbs_purchasing_document               string,     
 gfdwbs_text_of_purchase_ord              string,     
 gfdwbs_partner_cost_center               string,     
 gfdwbs_partner_cost_center_description   string,     
 gfdwbs_partner_internal_ord              string,     
 gfdwbs_partner_internal_ord_description  string,     
 gfdwbs_partner_wbs_element               string,     
 gfdwbs_partner_wbs_element_description   string,     
 gfdwbs_wbs_element                       string,     
 gfdwbs_wbs_element_description           string,     
 gfdwbs_actual_costs                      string,
ce_l1                                     string,
ce_desc_l1                                string,
ce_l2                                     string,
ce_desc_l2                                string,
ce_l3                                     string,
ce_desc_l3                                string,
ce_l4                                     string,
ce_desc_l4                                string,
ce_l5                                     string,
ce_desc_l5                                string,
ce_prefix                                 string,
ce_suffix                                 string,
ce                                        string,
ce_desc                                   string,
cost_type                                 string,
ce_ct_level_mapping_joined                string,
prjbt_cal_year_month_raw		          STRING,
prjbt_cal_year_month                      STRING,
prjbt_project			                  STRING,
prjbt_project_Name		                  STRING,
prjbt_bt_Responsibility_Key               STRING,
prjbt_bt_Responsibility_Name              STRING,
prjbt_bt_unit                             STRING,
prjbt_bt_unit_text                        STRING,
gfdwbs_posting_origin                     STRING   
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES 
(
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
);



insert into table final_gfd_project_wbs_flexible_joined
select 
wbs_stg1.*, 
stg_prj_bt_responsibility.*,
"PRJ" as gfdwbs_posting_origin
from wbs_stg1
left outer join
stg_prj_bt_responsibility
on gfdwbs_project_definition=prjbt_project
and gfdwbs_calendar_year_month=prjbt_cal_year_month;
---------------------------------------------------------------------------END---------------------

----Creation of one tof the staging table stg_prj_bt_responsibility from raw_prj_bt_responsibility-------------------


create table stg_prj_bt_responsibility ( 
 prjbt_cal_year_month_raw		     STRING
,prjbt_cal_year_month                STRING
,prjbt_project			             STRING
,prjbt_project_Name		             STRING
,prjbt_bt_Responsibility_Key         STRING
,prjbt_bt_Responsibility_Name        STRING
,prjbt_bt_unit                       STRING
,prjbt_bt_unit_text                  STRING
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   "serialization.null.format"="NULL"
   );


insert into table stg_prj_bt_responsibility
select
concat(split((split(cal_year_month,' ')[0]),'-')[1],'.',split((split(cal_year_month,' ')[0]),'-')[0]) as prjbt_cal_year_month_raw,
case when length(concat(split((split(cal_year_month,' ')[0]),'-')[1],'.',split((split(cal_year_month,' ')[0]),'-')[0])) > 6 
then concat(split((split(cal_year_month,' ')[0]),'-')[0],split((split(cal_year_month,' ')[0]),'-')[1]) 
else  concat(split((split(cal_year_month,' ')[0]),'-')[0],'0',split((split(cal_year_month,' ')[0]),'-')[1]) end as prjbt_cal_year_month, 
project as prjbt_project, 
project_name as prjbt_project_Name, 
bt_responsibility_key as prjbt_bt_Responsibility_Key, 
bt_responsibility_name as prjbt_bt_Responsibility_Name, 
split(substr(bt_responsibility_name,0,instr(bt_responsibility_name,' ')),':')[0] as prjbt_bt_unit,
regexp_replace((substr(bt_responsibility_name,instr(bt_responsibility_name,' '))),'\\-','') as prjbt_bt_unit_text
from raw_prj_bt_responsibility;
