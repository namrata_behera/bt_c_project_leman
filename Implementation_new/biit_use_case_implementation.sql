-------------------------Steps to reach one of the final table final_biit_transformed-----------------------------------------------------------------------------------------------

-------------------------Step 1: Creation of staging table from raw table raw_biit:------------------------------------------------------------------------

create table stg_BIIT (
 biit_data_entry_period_raw          string
,biit_Data_Entry_Period              string
,biit_cal_year_month_raw             string
,biit_cal_year_month                 string 
,biit_version                        string
,biit_reporting_unit_key             string
,biit_reporting_unit_text            string
,biit_bt_function_key                string
,biit_bt_function_text               string
,biit_cost_center_parent_key         string
,biit_cost_center_parent_text        string
,biit_bt_responsibility_key          string
,biit_bt_responsibility_text         string
,BIIT_BT_Unit                        string
,BIIT_BT_Unit_Text                   string   
,biit_partner_rep_unit               string
,biit_partner_rep_unit_text          string
,biit_posting_origin                 string
,biit_project_category               string
,biit_business                       string
,biit_project                        string
,biit_project_name                   string
,biit_partner_project                string
,biit_partner_project_name           string 
,biit_it_cost_center                 string
,biit_it_cost_center_text            string
,biit_IT_Cost_Center_Prefix          string
,biit_IT_Cost_Center_Code            string
,biit_business_division_function     string
,biit_partner_cost_center            string
,biit_partner_cost_center_text       string
,biit_function_project               string
,biit_position                       string
,biit_position_text                  string
,biit_cost_type_key                  string
,biit_cost_type_text                 string
,biit_opr_raw                        string
,biit_OPR                            string
,biit_fc_op_rate_yearend_raw         string
,biit_fc_op_rate_yearend             string   
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
   );


insert into table stg_BIIT 
select 
 lpad(biit_data_entry_period_raw,7,0) as BIIT_Data_Entry_Period_raw
,concat(substr((biit_data_entry_period_raw),4,4),substr((biit_data_entry_period_raw),0,2)) as BIIT_Data_Entry_Period
,lpad(biit_cal_year_month_raw,7,0) as BIIT_Cal_year_month_raw
,concat(substr((biit_cal_year_month_raw),4,4),substr((biit_cal_year_month_raw),0,2)) as BIIT_Cal_year_month
,BIIT_Version as  BIIT_Version                      
,BIIT_Reporting_Unit_Key as  BIIT_Reporting_Unit_Key         
,BIIT_Reporting_Unit_Text as BIIT_Reporting_Unit_Text            
,BIIT_BT_Function_Key as BIIT_BT_Function_Key
,BIIT_BT_Function_Text as     BIIT_BT_Function_Text          
,BIIT_Cost_Center_Parent_Key  as  BIIT_Cost_Center_Parent_Key      
,BIIT_Cost_Center_Parent_Text   as  BIIT_Cost_Center_Parent_Text    
,BIIT_BT_Responsibility_Key   as   BIIT_BT_Responsibility_Key     
,BIIT_BT_Responsibility_Text  as     BIIT_BT_Responsibility_Text   
,split(biit_bt_responsibility_text,'[ ]')[0] as BIIT_BT_Unit                        
,regexp_extract(biit_bt_responsibility_text,'\\s(.*)',0) as BIIT_BT_Unit_Text                  
,BIIT_Partner_Rep_Unit as   BIIT_Partner_Rep_Unit           
,BIIT_Partner_Rep_Unit_Text   as   BIIT_Partner_Rep_Unit_Text     
,BIIT_Posting_Origin     as      BIIT_Posting_Origin       
,BIIT_Project_Category    as       BIIT_Project_Category     
,BIIT_Business          as   BIIT_Business           
,BIIT_Project      as     BIIT_Project              
,BIIT_Project_Name    as  BIIT_Project_Name             
,BIIT_Partner_Project   as    BIIT_Partner_Project
,biit_partner_project_name  as  biit_partner_project_name    
,BIIT_IT_Cost_Center      as  BIIT_IT_Cost_Center          
,BIIT_IT_Cost_Center_Text  as       BIIT_IT_Cost_Center_Text    
,case 
WHEN biit_reporting_unit_key == '1500' then '0101'
END as BIIT_IT_Cost_Center_Prefix 
,concat(
 case WHEN biit_reporting_unit_key =='1500' then '0101' END,"/",biit_it_cost_center) as BIIT_IT_Cost_Center_Code        
,BIIT_Business_Division_Function as   BIIT_Business_Division_Function   
,BIIT_Partner_Cost_Center       as      BIIT_Partner_Cost_Center
,BIIT_Partner_Cost_Center_Text    as    BIIT_Partner_Cost_Center_Text
,BIIT_Function_Project         as      BIIT_Function_Project 
,BIIT_Position        as    BIIT_Position            
,BIIT_Position_Text    as    BIIT_Position_Text           
,BIIT_Cost_Type_Key        as   BIIT_Cost_Type_Key       
,BIIT_Cost_Type_Text    as     BIIT_Cost_Type_Text         
,BIIT_OPR_raw       as          BIIT_OPR_raw
,cast(biit_opr_raw as double)*1000   as BIIT_OPR
,BIIT_FC_OP_rate_YearEnd_raw as    BIIT_FC_OP_rate_YearEnd_raw          
,cast(BIIT_FC_OP_rate_YearEnd_raw as double)*1000 as BIIT_FC_OP_rate_YearEnd
from raw_biit
--------Comment while doing code review: We may use the filter condition: where biit_stg3.BIIT_Reporting_Unit_Key == '1500' instead of doing it in later stage.



-------------------------------------Step 2:Second staging table biit_cleaned_stg1 from stg_BIIT------------------------------------------------------------------------------

create table biit_cleaned_stg1 (
 biit_data_entry_period_raw          string
,biit_Data_Entry_Period              string
,biit_cal_year_month_raw             string
,biit_cal_year_month                 string 
,biit_version                        string
,biit_reporting_unit_key             string
,biit_reporting_unit_text            string
,biit_bt_function_key                string
,biit_bt_function_text               string
,biit_cost_center_parent_key         string
,biit_cost_center_parent_text        string
,biit_bt_responsibility_key          string
,biit_bt_responsibility_text         string
,BIIT_BT_Unit                        string
,BIIT_BT_Unit_Text                   string   
,biit_partner_rep_unit               string
,biit_partner_rep_unit_text          string
,biit_posting_origin                 string
,biit_project_category               string
,biit_business                       string
,biit_project                        string
,biit_project_name                   string
,biit_partner_project                string
,biit_partner_project_name           string 
,biit_it_cost_center                 string
,biit_it_cost_center_text            string
,biit_IT_Cost_Center_Prefix          string
,biit_IT_Cost_Center_Code            string
,biit_business_division_function     string
,biit_partner_cost_center            string
,biit_partner_cost_center_text       string
,biit_function_project               string
,biit_position                       string
,biit_position_text                  string
,biit_cost_type_key                  string
,biit_cost_type_text                 string
,biit_opr_raw                        string
,biit_OPR                            string
,biit_fc_op_rate_yearend_raw         string
,biit_fc_op_rate_yearend             string
,max_BIIT_Data_Entry_Period          string      
,biit_OPR_YTD                        string
,biit_OPR_YearEnd                    string    
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
   );


 insert into table  biit_cleaned_stg1
 select *,
 case when (BIIT_Data_Entry_Period = BIIT_CAL_YEAR_Month and biit_version = "Revised OP") 
 THEN BIIT_OPR
 ELSE NULL 
 END as BIIT_OPR_YTD,
 case WHEN (biit_version = "Revised OP" AND BIIT_CAL_YEAR_Month = "201712") 
 THEN BIIT_OPR 
 ELSE NULL 
 END as BIIT_OPR_YearEnd
 from
 stg_BIIT
 left join( select MAX(BIIT_Data_Entry_Period) as max_BIIT_Data_Entry_Period from stg_BIIT ) a
 where 
((BIIT_Cal_Year_Month = max_BIIT_Data_Entry_Period)
OR (BIIT_Cal_Year_Month = "201712" AND BIIT_Data_Entry_Period = max_BIIT_Data_Entry_Period));


-----Step 3: Creation of table biit_transformed_stg2-------------------------------------------------------------------

create table biit_transformed_stg2 (
BIIT_OPR_YearEnd                 string
,BIIT_FC_OP_rate_YearEnd         string
,BIIT_OPR_YTD                    string
,BIIT_Data_Entry_Period          string
,BIIT_Cal_year_month             string
,BIIT_Version                    string
,BIIT_Reporting_Unit_Key         string
,BIIT_Reporting_Unit_Text        string
,BIIT_BT_Function_Key            string
,BIIT_BT_Function_Text           string
,BIIT_Cost_Center_Parent_Key     string
,BIIT_Cost_Center_Parent_Text    string 
,BIIT_BT_Responsibility_Key      string
,BIIT_BT_Responsibility_Text     string
,BIIT_Posting_Origin             string
,BIIT_Project_Category           string
,BIIT_Business                   string
,BIIT_Project                    string
,BIIT_Project_Name               string
,BIIT_Partner_Project            string
,BIIT_IT_Cost_Center             string
,BIIT_IT_Cost_Center_Code        string
,BIIT_IT_Cost_Center_Text        string
,BIIT_Business_Division_Function string
,BIIT_Partner_Cost_Center        string
,BIIT_Partner_Cost_Center_Text   string
,BIIT_Function_Project           string
,BIIT_Position                   string
,BIIT_Position_Text              string
,BIIT_Cost_Type_Key              string
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
   );


insert into table biit_transformed_stg2
select SUM(cast(BIIT_OPR_YearEnd as double)) as BIIT_OPR_YearEnd,
SUM(cast(BIIT_FC_OP_rate_YearEnd as double)) AS BIIT_FC_OP_rate_YearEnd,
SUM(cast(BIIT_OPR_YTD as double)) as BIIT_OPR_YTD,
 BIIT_Data_Entry_Period
,BIIT_Cal_year_month
,BIIT_Version
,BIIT_Reporting_Unit_Key
,BIIT_Reporting_Unit_Text
,BIIT_BT_Function_Key
,BIIT_BT_Function_Text
,BIIT_Cost_Center_Parent_Key
,BIIT_Cost_Center_Parent_Text
,BIIT_BT_Responsibility_Key
,BIIT_BT_Responsibility_Text
,BIIT_Posting_Origin
,BIIT_Project_Category
,BIIT_Business
,BIIT_Project
,BIIT_Project_Name
,BIIT_Partner_Project
,BIIT_IT_Cost_Center
,BIIT_IT_Cost_Center_Code
,BIIT_IT_Cost_Center_Text
,BIIT_Business_Division_Function
,BIIT_Partner_Cost_Center
,BIIT_Partner_Cost_Center_Text
,BIIT_Function_Project
,BIIT_Position
,BIIT_Position_Text
,BIIT_Cost_Type_Key
from biit_cleaned_stg1
group by 
 BIIT_Data_Entry_Period
,BIIT_Cal_year_month
,BIIT_Version
,BIIT_Reporting_Unit_Key
,BIIT_Reporting_Unit_Text
,BIIT_BT_Function_Key
,BIIT_BT_Function_Text
,BIIT_Cost_Center_Parent_Key
,BIIT_Cost_Center_Parent_Text
,BIIT_BT_Responsibility_Key
,BIIT_BT_Responsibility_Text
,BIIT_Posting_Origin
,BIIT_Project_Category
,BIIT_Business
,BIIT_Project
,BIIT_Project_Name
,BIIT_Partner_Project
,BIIT_IT_Cost_Center
,BIIT_IT_Cost_Center_Code
,BIIT_IT_Cost_Center_Text
,BIIT_Business_Division_Function
,BIIT_Partner_Cost_Center
,BIIT_Partner_Cost_Center_Text
,BIIT_Function_Project
,BIIT_Position
,BIIT_Position_Text
,BIIT_Cost_Type_Key;


----------------Step 4: Creation of table biit_stg3-------------------------------------------------------------------------------

Create table biit_stg3 (
BIIT_OPR_YearEnd                  string
,BIIT_FC_OP_rate_YearEnd          string
,BIIT_OPR_YTD                     string
,BIIT_Data_Entry_Period           string
,BIIT_Cal_year_month              string
,BIIT_Version                     string
,BIIT_Reporting_Unit_Key          string
,BIIT_Reporting_Unit_Text         string
,BIIT_BT_Function_Key             string
,BIIT_BT_Function_Text            string
,BIIT_Cost_Center_Parent_Key      string
,BIIT_Cost_Center_Parent_Text     string 
,BIIT_BT_Responsibility_Key       string
,BIIT_BT_Responsibility_Text      string
,BIIT_Posting_Origin              string
,BIIT_Project_Category            string
,BIIT_Business                    string
,BIIT_Project                     string
,BIIT_Project_Name                string
,BIIT_Partner_Project             string
,BIIT_IT_Cost_Center              string
,BIIT_IT_Cost_Center_Code         string
,BIIT_IT_Cost_Center_Text         string
,BIIT_Business_Division_Function  string
,BIIT_Partner_Cost_Center         string
,BIIT_Partner_Cost_Center_Text    string
,BIIT_Function_Project            string
,BIIT_Position                    string
,BIIT_Position_Text               string
,BIIT_Cost_Type_Key               string
,ce_ct_cost_type_key              string
,cost_type_description            string
,c_ceh_mapping                    string
,c_ceh_mapping_level              string
,ce_ct_cost_type                  string
,level_mapping_joined             string
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
   );


insert into table biit_stg3
select 
biit_transformed_stg2.*, 
stg_ce_ct_mapping.ce_ct_cost_type_key,
stg_ce_ct_mapping.cost_type_description,
stg_ce_ct_mapping.c_ceh_mapping,
stg_ce_ct_mapping.c_ceh_mapping_level,
case
 when upper(BIIT_Posting_Origin) = 'PRJ' AND ce_ct_cost_type='bt_core_project' then NULL
 else ce_ct_cost_type
 end as cost_type,
stg_ce_ct_mapping.level_mapping_joined
from biit_transformed_stg2
left outer join
stg_ce_ct_mapping
on biit_cost_type_key=ce_ct_cost_type_key;


----Step 5: Creation of table biit_stg4---------------------------------------------------------

Create table biit_stg4 (
BIIT_OPR_YearEnd                  string
,BIIT_FC_OP_rate_YearEnd          string
,BIIT_OPR_YTD                     string
,BIIT_Data_Entry_Period           string
,BIIT_Cal_year_month              string
,BIIT_Version                     string
,BIIT_Reporting_Unit_Key          string
,BIIT_Reporting_Unit_Text         string
,BIIT_BT_Function_Key             string
,BIIT_BT_Function_Text            string
,BIIT_Cost_Center_Parent_Key      string
,BIIT_Cost_Center_Parent_Text     string 
,BIIT_BT_Responsibility_Key       string
,BIIT_BT_Responsibility_Text      string
,BIIT_Posting_Origin              string
,BIIT_Project_Category            string
,BIIT_Business                    string
,BIIT_Project                     string
,BIIT_Project_Name                string
,BIIT_Partner_Project             string
,BIIT_IT_Cost_Center              string
,BIIT_IT_Cost_Center_Code         string
,BIIT_IT_Cost_Center_Text         string
,BIIT_Business_Division_Function  string
,BIIT_Partner_Cost_Center         string
,BIIT_Partner_Cost_Center_Text    string
,BIIT_Function_Project            string
,BIIT_Position                    string
,BIIT_Position_Text               string
,BIIT_Cost_Type_Key               string
,ce_ct_cost_type_key              string
,cost_type_description            string
,c_ceh_mapping                    string
,c_ceh_mapping_level              string
,ce_ct_cost_type                  string
,level_mapping_joined             string
,Cost_Center_Code                 string
,CC_BT_Unit                       string
,BT_Unit_Text                     string
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
   );


insert into table biit_stg4
select biit_stg3.*,
cost_center_btunit_mapping.Cost_Center_Code,
trim(split(cost_center_btunit_mapping.BT_Unit,'[:]')[0]) as cc_bt_unit,
cost_center_btunit_mapping.BT_Unit_Text
from
biit_stg3
left outer join
cost_center_btunit_mapping
on biit_stg3.biit_it_cost_center_code=cost_center_btunit_mapping.Cost_Center_Code
where biit_stg3.BIIT_Reporting_Unit_Key == '1500'; ////////////////////////////////Comment during code review -we may take up this filter at earlier stage mentioned

---------Step 6: Creation of table biit_stg5----------------------------------------------------------------

Create table biit_stg5 (
BIIT_OPR_YearEnd                  string
,BIIT_FC_OP_rate_YearEnd          string
,BIIT_OPR_YTD                     string
,BIIT_Data_Entry_Period           string
,BIIT_Cal_year_month              string
,BIIT_Version                     string
,BIIT_Reporting_Unit_Key          string
,BIIT_Reporting_Unit_Text         string
,BIIT_BT_Function_Key             string
,BIIT_BT_Function_Text            string
,BIIT_Cost_Center_Parent_Key      string
,BIIT_Cost_Center_Parent_Text     string 
,BIIT_BT_Responsibility_Key       string
,BIIT_BT_Responsibility_Text      string
,BIIT_Posting_Origin              string
,BIIT_Project_Category            string
,BIIT_Business                    string
,BIIT_Project                     string
,BIIT_Project_Name                string
,BIIT_Partner_Project             string
,BIIT_IT_Cost_Center              string
,BIIT_IT_Cost_Center_Code         string
,BIIT_IT_Cost_Center_Text         string
,BIIT_Business_Division_Function  string
,BIIT_Partner_Cost_Center         string
,BIIT_Partner_Cost_Center_Text    string
,BIIT_Function_Project            string
,BIIT_Position                    string
,BIIT_Position_Text               string
,BIIT_Cost_Type_Key               string
,ce_ct_cost_type_key              string
,cost_type_description            string
,c_ceh_mapping                    string
,c_ceh_mapping_level              string
,ce_ct_cost_type                  string
,level_mapping_joined             string
,Cost_Center_Code                 string
,CC_BT_Unit                       string
,BT_Unit_Text                     string
,prjbt_cal_year_month_raw         string
,prjbt_cal_year_month             string
,prjbt_project                    string
,prjbt_project_name               string
,prjbt_bt_responsibility_key      string
,prjbt_bt_responsibility_name     string
,prjbt_bt_unit                    string
,prjbt_bt_unit_text               string
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
   );


insert into table biit_stg5
select biit_stg4.*
,stg_prj_bt_responsibility.prjbt_cal_year_month_raw
,stg_prj_bt_responsibility.prjbt_cal_year_month
,stg_prj_bt_responsibility.prjbt_project
,stg_prj_bt_responsibility.prjbt_project_name
,stg_prj_bt_responsibility.prjbt_bt_responsibility_key
,stg_prj_bt_responsibility.prjbt_bt_responsibility_name
,stg_prj_bt_responsibility.prjbt_bt_unit
,stg_prj_bt_responsibility.prjbt_bt_unit_text
from
biit_stg4
left outer join
stg_prj_bt_responsibility
on biit_stg4.BIIT_Cal_year_month=stg_prj_bt_responsibility.prjbt_cal_year_month
and biit_stg4.BIIT_Project=stg_prj_bt_responsibility.prjbt_project;


--------------Step 7: Creation of final table final_biit_transformed------------------------------------------------------

Create table final_biit_transformed (
BIIT_OPR_YearEnd                  string
,BIIT_FC_OP_rate_YearEnd          string
,BIIT_OPR_YTD                     string
,BIIT_Data_Entry_Period           string
,BIIT_Cal_year_month              string
,BIIT_Version                     string
,BIIT_Reporting_Unit_Key          string
,BIIT_Reporting_Unit_Text         string
,BIIT_BT_Function_Key             string
,BIIT_BT_Function_Text            string
,BIIT_Cost_Center_Parent_Key      string
,BIIT_Cost_Center_Parent_Text     string 
,BIIT_BT_Responsibility_Key       string
,BIIT_BT_Responsibility_Text      string
,BIIT_Posting_Origin              string
,BIIT_Project_Category            string
,BIIT_Business                    string
,BIIT_Project                     string
,BIIT_Project_Name                string
,BIIT_Partner_Project             string
,BIIT_IT_Cost_Center              string
,BIIT_IT_Cost_Center_Code         string
,BIIT_IT_Cost_Center_Text         string
,BIIT_Business_Division_Function  string
,BIIT_Partner_Cost_Center         string
,BIIT_Partner_Cost_Center_Text    string
,BIIT_Function_Project            string
,BIIT_Position                    string
,BIIT_Position_Text               string
,BIIT_Cost_Type_Key               string
,ce_ct_cost_type_key              string
,cost_type_description            string
,c_ceh_mapping                    string
,c_ceh_mapping_level              string
,cost_type                        string
,level_mapping_joined             string
,Cost_Center_Code                 string
,CC_BT_Unit                       string
,CC_BT_Unit_Text                  string
,prjbt_cal_year_month_raw         string
,prjbt_cal_year_month             string
,prjbt_project                    string
,prjbt_project_name               string
,prjbt_bt_responsibility_key      string
,prjbt_bt_responsibility_name     string
,prjbt_bt_unit                    string
,prjbt_bt_unit_text               string
,BIIT_BT_UNIT                     string
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
   );
   

insert into table final_biit_transformed
select 
 BIIT_OPR_YearEnd   as   BIIT_OPR_YearEnd             
,BIIT_FC_OP_rate_YearEnd    as     BIIT_FC_OP_rate_YearEnd  
,BIIT_OPR_YTD          as   BIIT_OPR_YTD         
,BIIT_Data_Entry_Period    as     BIIT_Data_Entry_Period   
,BIIT_Cal_year_month   as       BIIT_Cal_year_month     
,BIIT_Version       as            BIIT_Version   
,BIIT_Reporting_Unit_Key         as  BIIT_Reporting_Unit_Key
,BIIT_Reporting_Unit_Text      as    BIIT_Reporting_Unit_Text
,BIIT_BT_Function_Key     as     BIIT_BT_Function_Key    
,BIIT_BT_Function_Text     as   BIIT_BT_Function_Text     
,BIIT_Cost_Center_Parent_Key   as   BIIT_Cost_Center_Parent_Key 
,BIIT_Cost_Center_Parent_Text    as   BIIT_Cost_Center_Parent_Text
,BIIT_BT_Responsibility_Key   as    BIIT_BT_Responsibility_Key 
,BIIT_BT_Responsibility_Text    as   BIIT_BT_Responsibility_Text
,BIIT_Posting_Origin         as      BIIT_Posting_Origin
,BIIT_Project_Category      as     BIIT_Project_Category  
,BIIT_Business       as          BIIT_Business    
,BIIT_Project    as   BIIT_Project               
,BIIT_Project_Name   as   BIIT_Project_Name           
,BIIT_Partner_Project   as         BIIT_Partner_Project  
,BIIT_IT_Cost_Center    as      BIIT_IT_Cost_Center     
,BIIT_IT_Cost_Center_Code    as    BIIT_IT_Cost_Center_Code  
,BIIT_IT_Cost_Center_Text     as   BIIT_IT_Cost_Center_Text  
,BIIT_Business_Division_Function  as BIIT_Business_Division_Function
,BIIT_Partner_Cost_Center        as  BIIT_Partner_Cost_Center
,BIIT_Partner_Cost_Center_Text    as BIIT_Partner_Cost_Center_Text
,BIIT_Function_Project    as    BIIT_Function_Project     
,BIIT_Position    as         BIIT_Position        
,BIIT_Position_Text     as    BIIT_Position_Text       
,BIIT_Cost_Type_Key    as       BIIT_Cost_Type_Key     
,ce_ct_cost_type_key     as    ce_ct_cost_type_key      
,cost_type_description    as    cost_type_description     
,c_ceh_mapping       as    c_ceh_mapping          
,c_ceh_mapping_level     as       c_ceh_mapping_level   
,ce_ct_cost_type            as      cost_type       
,level_mapping_joined     as   level_mapping_joined      
,Cost_Center_Code as Cost_Center_Code
,CC_BT_Unit   as    CC_BT_Unit
,BT_Unit_Text    as    CC_BT_Unit_Text           
,prjbt_cal_year_month_raw       as prjbt_cal_year_month_raw  
,prjbt_cal_year_month    as    prjbt_cal_year_month      
,prjbt_project       as   prjbt_project           
,prjbt_project_name   as      prjbt_project_name       
,prjbt_bt_responsibility_key   as  prjbt_bt_responsibility_key  
,prjbt_bt_responsibility_name    as  prjbt_bt_responsibility_name
,prjbt_bt_unit        as     prjbt_bt_unit
,prjbt_bt_unit_text   as prjbt_bt_unit_text
,case when (CC_BT_Unit == '#' or  CC_BT_Unit == '')  then
prjbt_bt_unit
else CC_BT_Unit end as BIIT_BT_UNIT             
from
biit_stg5; 
   
