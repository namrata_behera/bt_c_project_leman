--------------Steps for creation of one of the final table final_gfd_cost_center_flexible_joined----------------------------------------------
---------------------Step 1: Creation of a level 2 staging table joining stg_ce_cost_element_hierarchy and stg_ce_ct_mapping------------------

create table stg_ce_h_ce_ct_l2
(
ce_l1 string,
ce_desc_l1 string,
ce_l2 string,
ce_desc_l2 string,
ce_l3 string,
ce_desc_l3 string,
ce_l4 string,
ce_desc_l4 string,
ce_l5 string,
ce_desc_l5 string,
ce_prefix string,
ce_suffix string,
ce string,
ce_desc string,
ce_h_cost_type string,
ce_ct_l2_cost_type string,
ce_ct_l2_level_mapping_joined string
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
   );


insert into table stg_ce_h_ce_ct_l2
select stg_ce_cost_element_hierarchy.*,
stg_ce_ct_mapping_d.cost_type,
stg_ce_ct_mapping_d.level_mapping_joined
from stg_ce_cost_element_hierarchy
left outer join (select distinct c_ceh_mapping, level_mapping_joined, cost_type from stg_ce_ct_mapping) as stg_ce_ct_mapping_d
on stg_ce_cost_element_hierarchy.ce_l2 = stg_ce_ct_mapping_d.c_ceh_mapping;



----Step 2: Creation of level 3 table----------------------------------


create table stg_ce_h_ce_ct_l3
(
ce_l1 string,
ce_desc_l1 string,
ce_l2 string,
ce_desc_l2 string,
ce_l3 string,
ce_desc_l3 string,
ce_l4 string,
ce_desc_l4 string,
ce_l5 string,
ce_desc_l5 string,
ce_prefix string,
ce_suffix string,
ce string,
ce_desc string,
ce_h_cost_type string,
ce_ct_l2_cost_type string,
ce_ct_l2_level_mapping_joined string,
ce_ct_l3_cost_type string,
ce_ct_l3_level_mapping_joined string
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
   );



insert into table stg_ce_h_ce_ct_l3
select stg_ce_h_ce_ct_l2.*,
stg_ce_ct_mapping_d.cost_type,
stg_ce_ct_mapping_d.level_mapping_joined
from stg_ce_h_ce_ct_l2
left outer join (select distinct c_ceh_mapping, level_mapping_joined, cost_type from stg_ce_ct_mapping) as stg_ce_ct_mapping_d
on stg_ce_h_ce_ct_l2.ce_l3 = stg_ce_ct_mapping_d.c_ceh_mapping;



----------Step 3: Creation of level 4 table------------------------------------------------

create table stg_ce_h_ce_ct_l4
(
ce_l1 string,
ce_desc_l1 string,
ce_l2 string,
ce_desc_l2 string,
ce_l3 string,
ce_desc_l3 string,
ce_l4 string,
ce_desc_l4 string,
ce_l5 string,
ce_desc_l5 string,
ce_prefix string,
ce_suffix string,
ce string,
ce_desc string,
ce_h_cost_type string,
ce_ct_l2_cost_type string,
ce_ct_l2_level_mapping_joined string,
ce_ct_l3_cost_type string,
ce_ct_l3_level_mapping_joined string,
ce_ct_l4_cost_type string,
ce_ct_l4_level_mapping_joined string
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
   );


insert into table stg_ce_h_ce_ct_l4
select stg_ce_h_ce_ct_l3.*,
stg_ce_ct_mapping_d.cost_type,
stg_ce_ct_mapping_d.level_mapping_joined
from stg_ce_h_ce_ct_l3
left outer join (select distinct c_ceh_mapping, level_mapping_joined, cost_type from stg_ce_ct_mapping) as stg_ce_ct_mapping_d
on stg_ce_h_ce_ct_l3.ce_l4 = stg_ce_ct_mapping_d.c_ceh_mapping;




---------Step 4:Creation of table taking level2,3,4 into account into one table------------------------------


create table stg_ce_h_ce_ct
(
ce_l1 string,
ce_desc_l1 string,
ce_l2 string,
ce_desc_l2 string,
ce_l3 string,
ce_desc_l3 string,
ce_l4 string,
ce_desc_l4 string,
ce_l5 string,
ce_desc_l5 string,
ce_prefix string,
ce_suffix string,
ce string,
ce_desc string,
cost_type string,
ce_ct_level_mapping_joined string
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
   );


insert into table stg_ce_h_ce_ct
select 
ce_l1,
ce_desc_l1,
ce_l2,
ce_desc_l2,
ce_l3,
ce_desc_l3,
ce_l4,
ce_desc_l4,
ce_l5,
ce_desc_l5,
ce_prefix,
ce_suffix,
ce,
ce_desc,
ce_h_cost_type as cost_type,
trim(concat(ce_ct_l2_level_mapping_joined,ce_ct_l3_level_mapping_joined,ce_ct_l4_level_mapping_joined)) as ce_ct_level_mapping_joined
from stg_ce_h_ce_ct_l4;




---------Step 5: creation of table joining raw_gfd_cost_center_flexible and stg_ce_h_ce_ct-------------------------------------

Create table joined_gfd_ce_flex_ce_h_ce_ct
(
gfdcc_calendar_year_month_raw string,
gfdcc_calendar_year_month string,
gfdcc_sender_receiver string,
gfdcc_sender_receiver_description  string,
gfdcc_local_company_code  string,
gfdcc_cost_center  string,
gfdcc_cost_center_code string,
gfdcc_cost_center_description string,
gfdcc_cost_element string,
gfdcc_cost_element_description string,
gfdcc_co_doc_header_txt string,
gfdcc_co_doc_line_item_txt string,
gfdcc_vendor  string,
gfdcc_vendor_description  string,
gfdcc_purchasing_document  string,
gfdcc_text_of_purchase_ord string,
gfdcc_partner_cost_center  string,
gfdcc_partner_cost_center_description string,
gfdcc_partner_internal_ord string,
gfdcc_partner_internal_ord_description string,
gfdcc_partner_wbs_element  string,
gfdcc_partner_wbs_element_description string,
gfdcc_partner_object string,
gfdcc_partner_object_type  string,
gfdcc_partner_object_type_description string,
gfdcc_actual_costs string,
ce_l1 string,
ce_desc_l1 string,
ce_l2 string,
ce_desc_l2 string,
ce_l3 string,
ce_desc_l3 string,
ce_l4 string,
ce_desc_l4 string,
ce_l5 string,
ce_desc_l5 string,
ce_prefix string,
ce_suffix string,
ce string,
ce_desc string,
cost_type string,
ce_ct_level_mapping_joined string
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES 
(
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
);


insert into table joined_gfd_ce_flex_ce_h_ce_ct
select
gfdcc_calendar_year_month_raw as gfdcc_calendar_year_month_raw, 
case when length(gfdcc_calendar_year_month_raw) > 6
then concat(split(gfdcc_calendar_year_month_raw,'[\.]')[1],split(gfdcc_calendar_year_month_raw,'[\.]')[0])
else  concat(split(gfdcc_calendar_year_month_raw,'[\.]')[1],'0',split(gfdcc_calendar_year_month_raw,'[\.]')[0]) end as gfdcc_calendar_year_month,
gfdcc_sender_receiver as gfdcc_sender_receiver,
gfdcc_sender_receiver_description  as gfdcc_sender_receiver_description,
gfdcc_local_company_code  as gfdcc_local_company_code,
gfdcc_cost_center  as gfdcc_cost_center,
gfdcc_cost_center_code as gfdcc_cost_center_code,
gfdcc_cost_center_description as gfdcc_cost_center_description,
gfdcc_cost_element as gfdcc_cost_element,
gfdcc_cost_element_description as gfdcc_cost_element_description,
gfdcc_co_doc_header_txt as gfdcc_co_doc_header_txt,
gfdcc_co_doc_line_item_txt as gfdcc_co_doc_line_item_txt,
gfdcc_vendor  as gfdcc_vendor,
gfdcc_vendor_description  as gfdcc_vendor_description,
gfdcc_purchasing_document  as gfdcc_purchasing_document,
gfdcc_text_of_purchase_ord as gfdcc_text_of_purchase_ord,
gfdcc_partner_cost_center  as gfdcc_partner_cost_center,
gfdcc_partner_cost_center_description as gfdcc_partner_cost_center_description,
gfdcc_partner_internal_ord as gfdcc_partner_internal_ord,
gfdcc_partner_internal_ord_description as gfdcc_partner_internal_ord_description,
gfdcc_partner_wbs_element  as gfdcc_partner_wbs_element,
gfdcc_partner_wbs_element_description as gfdcc_partner_wbs_element_description,
gfdcc_partner_object as gfdcc_partner_object,
gfdcc_partner_object_type  as gfdcc_partner_object_type,
gfdcc_partner_object_type_description as gfdcc_partner_object_type_description,
gfdcc_actual_costs as gfdcc_actual_costs,
stg_ce_h_ce_ct.* 
from raw_gfd_cost_center_flexible left outer join stg_ce_h_ce_ct on raw_gfd_cost_center_flexible.gfdcc_cost_element = stg_ce_h_ce_ct.ce;


-----Step 6:Creation of table cost_center_btunit_mapping from stg_BIIT------------------------------------------------------------------------------------

create table cost_center_btunit_mapping
(
 Cost_Center_Code  string,
 BT_Unit  string,
 BT_Unit_Text  string
 )
 ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES 
(
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
);


insert into table cost_center_btunit_mapping 
select BIIT_IT_Cost_Center_Code AS Cost_Center_Code,
min(trim(split(BIIT_BT_Unit,'[:]')[0])) AS BT_Unit,
min(BIIT_BT_Unit_Text) AS BT_Unit_Text
from stg_BIIT
where (BIIT_IT_Cost_Center_Code != '0101/#' and BIIT_IT_Cost_Center_Code != '#/#' and BIIT_IT_Cost_Center_Code != '#')
GROUP BY BIIT_IT_Cost_Center_Code;



-------Step 7: Creation of final table final_gfd_cost_center_flexible_joined---------------------------------


create table final_gfd_cost_center_flexible_joined
(
gfdcc_calendar_year_month_raw string,
gfdcc_calendar_year_month string,
gfdcc_sender_receiver string,
gfdcc_sender_receiver_description  string,
gfdcc_local_company_code  string,
gfdcc_cost_center  string,
gfdcc_cost_center_code string,
gfdcc_cost_center_description string,
gfdcc_cost_element string,
gfdcc_cost_element_description string,
gfdcc_co_doc_header_txt string,
gfdcc_co_doc_line_item_txt string,
gfdcc_vendor  string,
gfdcc_vendor_description  string,
gfdcc_purchasing_document  string,
gfdcc_text_of_purchase_ord string,
gfdcc_partner_cost_center  string,
gfdcc_partner_cost_center_description string,
gfdcc_partner_internal_ord string,
gfdcc_partner_internal_ord_description string,
gfdcc_partner_wbs_element  string,
gfdcc_partner_wbs_element_description string,
gfdcc_partner_object string,
gfdcc_partner_object_type  string,
gfdcc_partner_object_type_description string,
gfdcc_actual_costs string,
ce_l1 string,
ce_desc_l1 string,
ce_l2 string,
ce_desc_l2 string,
ce_l3 string,
ce_desc_l3 string,
ce_l4 string,
ce_desc_l4 string,
ce_l5 string,
ce_desc_l5 string,
ce_prefix string,
ce_suffix string,
ce string,
ce_desc string,
cost_type string,
ce_ct_level_mapping_joined string,
Cost_Center_Code  string,
BT_Unit  string,
BT_Unit_Text  string,
gfdcc_posting_origin  string
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES 
(
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
);


insert into table final_gfd_cost_center_flexible_joined
select joined_gfd_ce_flex_ce_h_ce_ct.*,
cost_center_btunit_mapping.*,
"CC" as gfdcc_posting_origin
from joined_gfd_ce_flex_ce_h_ce_ct
left outer join
cost_center_btunit_mapping
on 
gfdcc_cost_center=Cost_Center_Code;

