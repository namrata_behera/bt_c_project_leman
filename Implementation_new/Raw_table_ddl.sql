drop table raw_biit;
create table raw_biit( 
 biit_data_entry_period_raw          string
,biit_cal_year_month_raw             string
,biit_version                        string
,biit_reporting_unit_key             string
,biit_reporting_unit_text            string
,biit_bt_function_key                string
,biit_bt_function_text               string
,biit_cost_center_parent_key         string
,biit_cost_center_parent_text        string
,biit_bt_responsibility_key          string
,biit_bt_responsibility_text         string
,biit_partner_rep_unit               string
,biit_partner_rep_unit_text          string
,biit_posting_origin                 string
,biit_project_category               string
,biit_business                       string
,biit_project                        string
,biit_project_name                   string
,biit_partner_project                string
,biit_partner_project_name           string 
,biit_it_cost_center                 string
,biit_it_cost_center_text            string
,biit_business_division_function     string
,biit_partner_cost_center            string
,biit_partner_cost_center_text       string
,biit_function_project               string
,biit_position                       string
,biit_position_text                  string
,biit_cost_type_key                  string
,biit_cost_type_text                 string
,biit_opr_raw                        string
,biit_fc_op_rate_yearend_raw         string
) 
row format serde 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
with serdeproperties (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   "serialization.null.format"="NULL"
)
tblproperties("skip.header.line.count"="1");

load data local inpath '/data/x169145/project-leman/biit_biit.csv' into table raw_biit;


drop table raw_gfd_cost_center_flexible;
CREATE TABLE raw_gfd_cost_center_flexible ( 
 gfdcc_calendar_year_month_raw                STRING
,gfdcc_sender_receiver                        STRING
,gfdcc_sender_receiver_description            STRING
,gfdcc_local_company_code                     STRING
,gfdcc_cost_center                            STRING
,gfdcc_cost_center_code                       STRING
,gfdcc_cost_center_description                STRING
,gfdcc_cost_element                           STRING
,gfdcc_cost_element_description               STRING
,gfdcc_co_doc_header_txt                      STRING
,gfdcc_co_doc_line_item_txt                   STRING
,gfdcc_vendor                                 STRING
,gfdcc_vendor_description                     STRING
,gfdcc_purchasing_document                    BIGINT
,gfdcc_text_of_purchase_ord                   STRING
,gfdcc_partner_cost_center                    STRING
,gfdcc_partner_cost_center_description        STRING
,gfdcc_partner_internal_ord                   STRING
,gfdcc_partner_internal_ord_description       STRING
,gfdcc_partner_wbs_element                    STRING
,gfdcc_partner_wbs_element_description        STRING
,gfdcc_partner_object                         STRING
,gfdcc_partner_object_type                    STRING
,gfdcc_partner_object_type_description        STRING
,gfdcc_actual_costs                           STRING
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   "serialization.null.format"="NULL"
   )
TBLPROPERTIES("skip.header.line.count"="1");

load data local inpath '/data/x169145/project-leman/gfd_cost_center_flexible.csv' into table raw_gfd_cost_center_flexible;

load data local inpath '/data/x188210/gfd_cost_center_flexible_updated.csv' into table raw_gfd_cost_center_flexible;--------------new data load(28953 records now)




CREATE TABLE raw_gfd_project_wbs_flexible ( 
 gfdwbs_calendar_year_month_raw               STRING
,gfdwbs_sender_receiver                       STRING
,gfdwbs_project_definition                    STRING
,gfdwbs_project_description                   STRING
,gfdwbs_cost_element                          STRING
,gfdwbs_cost_element_description              STRING
,gfdwbs_co_doc_header_txt                     STRING
,gfdwbs_co_doc_line_item_txt                  STRING
,gfdwbs_vendor                                STRING
,gfdwbs_vendor_description                    STRING
,gfdwbs_purchasing_document                   STRING
,gfdwbs_text_of_purchase_ord                  STRING
,gfdwbs_partner_cost_center                   STRING
,gfdwbs_partner_cost_center_description       STRING
,gfdwbs_partner_internal_ord                  STRING
,gfdwbs_partner_internal_ord_description      STRING
,gfdwbs_partner_wbs_element                   STRING
,gfdwbs_partner_wbs_element_description       STRING
,gfdwbs_wbs_element                           STRING
,gfdwbs_wbs_element_description               STRING
,gfdwbs_actual_costs                          STRING
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   "serialization.null.format"="NULL"
   )
TBLPROPERTIES("skip.header.line.count"="1");


load data local inpath '/data/x169145/project-leman/gfd_project_wbs_flexible.csv' into table raw_gfd_project_wbs_flexible;




drop table raw_ppm_obligo;
CREATE TABLE raw_ppm_obligo ( 
 ppm_category                                 STRING
,ppm_category_text                            STRING
,ppm_supplier_po                              STRING
,ppm_supplier_po_text                         STRING
,ppm_po_creation_date_raw                     STRING
,ppm_po_id                                    STRING
,ppm_po_line_id                               STRING
,ppm_po_item_text                             STRING
,ppm_cost_center                              STRING
,ppm_cost_center_parent_key                   STRING 
,ppm_wbs_element                              STRING
,ppm_wbs_element_text                         STRING
,ppm_gl_account                               STRING
,ppm_final_invoice_flag                       STRING
,ppm_planned_po_delivery_date                 STRING
,ppm_purchase_order_value_lc                  STRING
,ppm_cost_value_po_currency                   STRING
,ppm_purchase_order_value_gc                  STRING
,ppm_spend_lc                                 STRING
,ppm_spend_gc                                 STRING
,ppm_open_amount_lc                           STRING
,ppm_open_amount_gc                           STRING
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   "serialization.null.format"="NULL"
   )

TBLPROPERTIES("skip.header.line.count"="1");--------------made it 2


load data local inpath '/data/x169145/project-leman/ppm_obligo.csv' into table raw_ppm_obligo;

load data local inpath '/data/x188210/ppm_obligo.csv' into table raw_ppm_obligo; 






CREATE TABLE raw_ce_ct_mapping ( 
 cost_Type		             STRING
,cost_Type_Description       STRING
,c_ceh_mapping				 STRING
,C_CEH_MAPPING_LEVEL         STRING
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   "serialization.null.format"="NULL"
   )
TBLPROPERTIES("skip.header.line.count"="1");

load data local inpath '/data/x169145/project-leman/ce_ct_mapping_raw.csv' into table raw_ce_ct_mapping;





CREATE TABLE raw_ce_cost_element_hierarchy ( 
 cost_Element_Level_1				 STRING
,cost_Element_Description_Level_1	 STRING
,cost_Element_Level_2	             STRING
,cost_Element_Description_Level_2	 STRING
,cost_Element_Level_3	             STRING
,cost_Element_Description_Level_3	 STRING
,cost_Element_Level_4		         STRING
,cost_Element_Description_Level_4	 STRING
,cost_Element_Level_5		         STRING
,cost_Element_Description_Level_5	 STRING
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   "serialization.null.format"="NULL"
   )
TBLPROPERTIES("skip.header.line.count"="1");

load data local inpath '/data/x169145/project-leman/ce_cost_element_hierarchy.csv' into table raw_ce_cost_element_hierarchy;



drop table raw_prj_bt_responsibility;
create table raw_prj_bt_responsibility ( 
 cal_year_month					     STRING
,project				             STRING
,project_Name			             STRING
,bt_Responsibility_Key	             STRING
,bt_Responsibility_Name	             STRING
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   "serialization.null.format"="NULL"
   )
TBLPROPERTIES("skip.header.line.count"="1");

load data local inpath '/data/x169145/project-leman/prj_to_bt_responsibility.csv' into table raw_prj_bt_responsibility;

load data local inpath '/data/x188210/prj_to_bt_responsibility.csv' into table raw_prj_bt_responsibility;


