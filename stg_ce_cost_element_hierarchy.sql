----------------------------------One of the staging table(with derived columns from columns of raw table) from raw table- raw_ce_cost_element_hierarchy-------------------------------

drop table stg_ce_cost_element_hierarchy;
create table stg_ce_cost_element_hierarchy (
ce_l1 string,
ce_desc_l1 string,
ce_l2 string,
ce_desc_l2 string,
ce_l3 string,
ce_desc_l3 string,
ce_l4 string,
ce_desc_l4 string,
ce_l5 string,
ce_desc_l5 string,
ce_prefix string,
ce_suffix string,
ce string,
ce_desc string,
cost_type string
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   
   "serialization.null.format"="NULL"
   )


insert into table stg_ce_cost_element_hierarchy
select 
case when trim(cost_element_level_1) = '' or upper(cost_element_level_1) = 'NULL' then NULL else cost_element_level_1 end as ce_l1,
case when trim(cost_element_description_level_1) = '' or upper(cost_element_description_level_1) = 'NULL' then NULL else cost_element_description_level_1 end as ce_desc_l1,
case when trim(cost_element_level_2) = '' or upper(cost_element_level_2) = 'NULL' then NULL else REGEXP_REPLACE(cost_element_level_2, "^00+", '') end as ce_l2,
case when trim(cost_element_description_level_2) = '' or upper(cost_element_description_level_2) = 'NULL' then NULL else cost_element_description_level_2 end as ce_desc_l2,
case when trim(cost_element_level_3) = '' or upper(cost_element_level_3) = 'NULL' then NULL else REGEXP_REPLACE(cost_element_level_3, "^00+", '') end as ce_l3,
case when trim(cost_element_description_level_3) = '' or upper(cost_element_description_level_3) = 'NULL' then NULL else cost_element_description_level_3 end as ce_desc_l3,
case when trim(cost_element_level_4) = '' or upper(cost_element_level_4) = 'NULL' then NULL else REGEXP_REPLACE(cost_element_level_4, "^00+", '') end as ce_l4,
case when trim(cost_element_description_level_4) = '' or upper(cost_element_description_level_4) = 'NULL' then NULL else cost_element_description_level_4 end as ce_desc_l4,
case when trim(cost_element_level_5) = '' or upper(cost_element_level_5) = 'NULL' then NULL else cost_element_level_5 end as ce_l5,
case when trim(cost_element_description_level_5) = '' or upper(cost_element_description_level_5) = 'NULL' then NULL else cost_element_description_level_5 end as ce_desc_l5,
case 
 when trim(cost_element_level_5) = '' or upper(cost_element_level_5) = 'NULL' or cost_element_level_5 IS NULL then
  (case when length(cost_element_level_4) - length(REGEXP_REPLACE(cost_element_level_4, "^00+", '')) > 1 then NULL
    when trim(cost_element_level_4) = '' or upper(cost_element_level_4) = 'NULL' then NULL
    else substr(cost_element_level_4, 1, 4)
    end
  )
else substr(cost_element_level_5, 1, 4)
end as ce_prefix,
case 
 when trim(cost_element_level_5) = '' or upper(cost_element_level_5) = 'NULL' or cost_element_level_5 IS NULL then
  (case when length(cost_element_level_4) - length(REGEXP_REPLACE(cost_element_level_4, "^00+", '')) > 1 then NULL
    when trim(cost_element_level_4) = '' or upper(cost_element_level_4) = 'NULL' then NULL
    else REGEXP_REPLACE(substr(cost_element_level_4, 5), "^0+", '')
    end
  )
else REGEXP_REPLACE(substr(cost_element_level_5, 5), "^0+", '')
end as ce_suffix,
case 
 when trim(cost_element_level_5) = '' or upper(cost_element_level_5) = 'NULL' or cost_element_level_5 IS NULL then
  (case when length(cost_element_level_4) - length(REGEXP_REPLACE(cost_element_level_4, "^00+", '')) > 1 then NULL
    when trim(cost_element_level_4) = '' or upper(cost_element_level_4) = 'NULL' then NULL
    else concat(substr(cost_element_level_4, 1, 4),"/",REGEXP_REPLACE(substr(cost_element_level_4, 5), "^0+",''))
    end
  )
else concat(substr(cost_element_level_5, 1, 4),"/",REGEXP_REPLACE(substr(cost_element_level_5, 5), "^0+", ''))
end as ce,
case 
 when trim(cost_element_level_5) = '' or upper(cost_element_level_5) = 'NULL' or cost_element_level_5 IS NULL then
  (case when length(cost_element_level_4) - length(REGEXP_REPLACE(cost_element_level_4, "^00+", '')) > 1 then NULL
    when trim(cost_element_level_4) = '' or upper(cost_element_level_4) = 'NULL' then NULL
    else cost_element_description_level_4
    end
  )
else cost_element_description_level_5
end as ce_desc,
case
 when REGEXP_REPLACE(cost_element_level_3, "^00+", '') = '180400' then 'bt_core_project'
 when REGEXP_REPLACE(cost_element_level_3, "^00+", '') in ('180600', '180700') then NULL
 else 'primary_cost' end as cost_type
from raw_ce_cost_element_hierarchy;


