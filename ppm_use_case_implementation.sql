-----Steps for creation of final PPM table-----------------------------------
-----Step 1: Creation of stg_ppm_obligo from raw_ppm_obligo-------------

CREATE TABLE stg_ppm_obligo ( 
 ppm_category                                 STRING
,ppm_category_text                            STRING
,ppm_supplier_po                              STRING
,ppm_supplier_po_text                         STRING
,ppm_po_creation_date_raw                     STRING
,ppm_po_id                                    STRING
,ppm_po_line_id                               STRING
,ppm_po_item_text                             STRING
,ppm_cost_center                              STRING
,ppm_cost_center_parent_key                   STRING 
,ppm_wbs_element                              STRING
,ppm_wbs_element_mapping                      STRING
,ppm_wbs_element_text                         STRING
,ppm_gl_account                               STRING
,ppm_cost_element                             STRING 
,ppm_final_invoice_flag                       STRING
,ppm_planned_po_delivery_date                 STRING
,ppm_po_calendar_year_month                   STRING
,ppm_purchase_order_value_lc                  STRING
,ppm_cost_value_po_currency                   STRING
,ppm_purchase_order_value_gc                  STRING
,ppm_spend_lc                                 STRING
,ppm_spend_gc                                 STRING
,ppm_open_amount_lc                           STRING
,ppm_open_amount_gc                           STRING
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   "serialization.null.format"="NULL"
   );


insert into TABLE stg_ppm_obligo 
select  
 ppm_category as    ppm_category
,ppm_category_text   as      ppm_category_text
,ppm_supplier_po   as    ppm_supplier_po
,ppm_supplier_po_text  as   ppm_supplier_po_text
,split(ppm_po_creation_date_raw,'[ ]')[0] as ppm_po_creation_date_raw
,ppm_po_id  as  ppm_po_id
,ppm_po_line_id    as   ppm_po_line_id
,ppm_po_item_text  as  ppm_po_item_text
,ppm_cost_center    as  ppm_cost_center
,ppm_cost_center_parent_key  as   ppm_cost_center_parent_key 
,ppm_wbs_element   as   ppm_wbs_element
,concat(split(ppm_wbs_element,'[.]')[0],'.',split(ppm_wbs_element,'[.]')[1]) as ppm_wbs_element_mapping
,ppm_wbs_element_text as  ppm_wbs_element_text 
,ppm_gl_account as ppm_gl_account
,concat(split(ppm_cost_center,'[/]')[0],"/",split(ppm_gl_account,'[/]')[1]) as ppm_cost_element
,ppm_final_invoice_flag  as    ppm_final_invoice_flag
,ppm_planned_po_delivery_date    as    ppm_planned_po_delivery_date
,concat(split(ppm_po_creation_date_raw,'[\-]')[0],split(ppm_po_creation_date_raw,'[\-]')[1]) as ppm_po_calendar_year_month 
,ppm_purchase_order_value_lc as  ppm_purchase_order_value_lc
,ppm_cost_value_po_currency      as   ppm_cost_value_po_currency
,ppm_purchase_order_value_gc   as   ppm_purchase_order_value_gc
,ppm_spend_lc    as     ppm_spend_lc
,ppm_spend_gc     as  ppm_spend_gc
,ppm_open_amount_lc      as   ppm_open_amount_lc
,ppm_open_amount_gc   as  ppm_open_amount_gc
from raw_ppm_obligo;


--------Step 2: Creation of ppm_stg1-------------------


CREATE TABLE ppm_stg1 ( 
 ppm_category                                 STRING
,ppm_category_text                            STRING
,ppm_supplier_po                              STRING
,ppm_supplier_po_text                         STRING
,ppm_po_creation_date_raw                     STRING
,ppm_po_id                                    STRING
,ppm_po_line_id                               STRING
,ppm_po_item_text                             STRING
,ppm_cost_center                              STRING
,ppm_cost_center_parent_key                   STRING 
,ppm_wbs_element                              STRING
,ppm_wbs_element_mapping                      STRING     
,ppm_wbs_element_text                         STRING
,ppm_gl_account                               STRING
,ppm_cost_element                             STRING 
,ppm_final_invoice_flag                       STRING
,ppm_planned_po_delivery_date                 STRING
,ppm_po_calendar_year_month                   STRING
,ppm_purchase_order_value_lc                  STRING
,ppm_cost_value_po_currency                   STRING
,ppm_purchase_order_value_gc                  STRING
,ppm_spend_lc                                 STRING
,ppm_spend_gc                                 STRING
,ppm_open_amount_lc                           STRING
,ppm_open_amount_gc                           STRING
,ce_l1                                        string
,ce_desc_l1                                   string
,ce_l2                                        string
,ce_desc_l2                                   string
,ce_l3                                        string
,ce_desc_l3                                   string
,ce_l4                                        string
,ce_desc_l4                                   string
,ce_l5                                        string
,ce_desc_l5                                   string
,ce_prefix                                    string
,ce_suffix                                    string
,ce                                           string
,ce_desc                                      string
,cost_type                                    string
,ce_ct_level_mapping_joined                   string
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   "serialization.null.format"="NULL"
   );


insert into table ppm_stg1
select 
stg_ppm_obligo.*,
stg_ce_h_ce_ct.ce_l1,
stg_ce_h_ce_ct.ce_desc_l1,
stg_ce_h_ce_ct.ce_l2,
stg_ce_h_ce_ct.ce_desc_l2,
stg_ce_h_ce_ct.ce_l3,
stg_ce_h_ce_ct.ce_desc_l3,
stg_ce_h_ce_ct.ce_l4,
stg_ce_h_ce_ct.ce_desc_l4,
stg_ce_h_ce_ct.ce_l5,
stg_ce_h_ce_ct.ce_desc_l5,
stg_ce_h_ce_ct.ce_prefix,
stg_ce_h_ce_ct.ce_suffix,
stg_ce_h_ce_ct.ce,
stg_ce_h_ce_ct.ce_desc,
case 
when trim(ppm_wbs_element) != '' or upper(ppm_wbs_element) != '#' or ppm_wbs_element IS NOT NULL then
cost_type
else 'bt_core_project'
 end as cost_type,
stg_ce_h_ce_ct.ce_ct_level_mapping_joined
from stg_ppm_obligo 
left outer join 
stg_ce_h_ce_ct on 
stg_ppm_obligo.ppm_cost_element = stg_ce_h_ce_ct.ce;

-----Step 3: Creation of table ppm_stg2----------------------



CREATE TABLE ppm_stg2 ( 
 ppm_category                                 STRING
,ppm_category_text                            STRING
,ppm_supplier_po                              STRING
,ppm_supplier_po_text                         STRING
,ppm_po_creation_date_raw                     STRING
,ppm_po_id                                    STRING
,ppm_po_line_id                               STRING
,ppm_po_item_text                             STRING
,ppm_cost_center                              STRING
,ppm_cost_center_parent_key                   STRING 
,ppm_wbs_element                              STRING
,ppm_wbs_element_mapping                      STRING     
,ppm_wbs_element_text                         STRING
,ppm_gl_account                               STRING
,ppm_cost_element                             STRING 
,ppm_final_invoice_flag                       STRING
,ppm_planned_po_delivery_date                 STRING
,ppm_po_calendar_year_month                   STRING
,ppm_purchase_order_value_lc                  STRING
,ppm_cost_value_po_currency                   STRING
,ppm_purchase_order_value_gc                  STRING
,ppm_spend_lc                                 STRING
,ppm_spend_gc                                 STRING
,ppm_open_amount_lc                           STRING
,ppm_open_amount_gc                           STRING
,ce_l1                                        STRING
,ce_desc_l1                                   STRING
,ce_l2                                        STRING
,ce_desc_l2                                   STRING
,ce_l3                                        STRING
,ce_desc_l3                                   STRING
,ce_l4                                        STRING
,ce_desc_l4                                   STRING
,ce_l5                                        STRING
,ce_desc_l5                                   STRING
,ce_prefix                                    STRING
,ce_suffix                                    STRING
,ce                                           STRING
,ce_desc                                      STRING
,cost_type                                    STRING
,ce_ct_level_mapping_joined                   STRING
,prjbt_cal_year_month_raw		              STRING
,prjbt_cal_year_month                         STRING
,prjbt_project			                      STRING
,prjbt_project_Name		                      STRING
,prjbt_bt_Responsibility_Key                  STRING
,prjbt_bt_Responsibility_Name                 STRING
,prjbt_bt_unit                                STRING
,prjbt_bt_unit_text                           STRING
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   "serialization.null.format"="NULL"
   );


insert into table ppm_stg2
select ppm_stg1.*, 
stg_prj_bt_responsibility.*
from ppm_stg1
left outer join
stg_prj_bt_responsibility
on ppm_wbs_element_mapping=prjbt_project
and ppm_po_calendar_year_month=prjbt_cal_year_month;

--------------------Step 4: Creation of final PPM table final_ppm_po_flexible_joined---------------------------


CREATE TABLE final_ppm_po_flexible_joined ( 
 ppm_category                                 STRING
,ppm_category_text                            STRING
,ppm_supplier_po                              STRING
,ppm_supplier_po_text                         STRING
,ppm_po_creation_date_raw                     STRING
,ppm_po_id                                    STRING
,ppm_po_line_id                               STRING
,ppm_po_item_text                             STRING
,ppm_cost_center                              STRING
,ppm_cost_center_parent_key                   STRING 
,ppm_wbs_element                              STRING
,ppm_wbs_element_mapping                      STRING     
,ppm_wbs_element_text                         STRING
,ppm_gl_account                               STRING
,ppm_cost_element                             STRING 
,ppm_final_invoice_flag                       STRING
,ppm_planned_po_delivery_date                 STRING
,ppm_po_calendar_year_month                   STRING
,ppm_purchase_order_value_lc                  STRING
,ppm_cost_value_po_currency                   STRING
,ppm_purchase_order_value_gc                  STRING
,ppm_spend_lc                                 STRING
,ppm_spend_gc                                 STRING
,ppm_open_amount_lc                           STRING
,ppm_open_amount_gc                           STRING
,ce_l1                                        STRING
,ce_desc_l1                                   STRING
,ce_l2                                        STRING
,ce_desc_l2                                   STRING
,ce_l3                                        STRING
,ce_desc_l3                                   STRING
,ce_l4                                        STRING
,ce_desc_l4                                   STRING
,ce_l5                                        STRING
,ce_desc_l5                                   STRING
,ce_prefix                                    STRING
,ce_suffix                                    STRING
,ce                                           STRING
,ce_desc                                      STRING
,cost_type                                    STRING
,ce_ct_level_mapping_joined                   STRING
,prjbt_cal_year_month_raw		              STRING
,prjbt_cal_year_month                         STRING
,prjbt_project			                      STRING
,prjbt_project_Name		                      STRING
,prjbt_bt_Responsibility_Key                  STRING
,prjbt_bt_Responsibility_Name                 STRING
,prjbt_bt_unit                                STRING
,prjbt_bt_unit_text                           STRING
,CC_Cost_Center_Code                          string
,PPM_BT_UNIT                                  string
,CC_BT_Unit_Text                              string
,ppm_posting_origin                           string 
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\"",
   "serialization.null.format"="NULL"
   );


insert into table final_ppm_po_flexible_joined
select ppm_stg2.*,
cost_center_btunit_mapping.Cost_Center_Code as CC_Cost_Center_Code,
case
when (ppm_stg2.prjbt_bt_unit != '#' AND trim(ppm_stg2.prjbt_bt_unit) != '' AND upper(ppm_stg2.prjbt_bt_unit) != 'NULL' AND ppm_stg2.prjbt_bt_unit IS NOT NULL) then ppm_stg2.prjbt_bt_unit
when (cost_center_btunit_mapping.BT_Unit != '#' AND trim(cost_center_btunit_mapping.BT_Unit) != '' AND upper(cost_center_btunit_mapping.BT_Unit) != 'NULL' AND cost_center_btunit_mapping.BT_Unit is NOT NULL) then cost_center_btunit_mapping.BT_Unit
else NULL
end as PPM_BT_UNIT,
cost_center_btunit_mapping.BT_Unit_Text as CC_BT_Unit_Text,
case
when (ppm_stg2.ppm_wbs_element_mapping != '#' AND trim(ppm_stg2.ppm_wbs_element_mapping) != '' AND upper(ppm_stg2.ppm_wbs_element_mapping) != 'NULL' AND ppm_stg2.ppm_wbs_element_mapping IS NOT NULL) then "PRJ"
when (ppm_stg2.ppm_cost_center != '#' AND ppm_stg2.ppm_cost_center != '0101/#' AND ppm_stg2.ppm_cost_center != '#/#' AND trim(ppm_stg2.ppm_cost_center) != '' AND upper(ppm_stg2.ppm_cost_center) != 'NULL' AND ppm_stg2.ppm_cost_center is NOT NULL) then "CC"
else NULL
end as ppm_posting_origin
from
ppm_stg2
left outer join
cost_center_btunit_mapping
on ppm_stg2.ppm_cost_center=cost_center_btunit_mapping.Cost_Center_Code;  

